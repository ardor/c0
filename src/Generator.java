import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Generator implements Runnable{
    private final long effectiveBalanceFXT;
    private final byte[] publicKey = new byte[0x20];

    private final State state;
    boolean quit = false;

    public Generator(State platformState, long effectiveBalanceFXT) {
        state = platformState;
        this.effectiveBalanceFXT = effectiveBalanceFXT;

        Random random = new Random();
        random.nextBytes(publicKey);
    }

    public void stop() {
        quit = true;
    }

    public void run() {
        while(!quit) {
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {/* empty*/}

            if(state.timeToGenerate(publicKey, effectiveBalanceFXT) == 0) {
                state.submitBlock(generateBestBlock(2));
            }
        }

    }

    private State.Block generateBestBlock(int count) {
        State.Block block = null;

        List<State.Block> list = state.getBlocksLast(count);

        for(State.Block stateBlock : list) {
            State.Block localBlock = state.generateBlock(stateBlock, publicKey, effectiveBalanceFXT); // effective balance would have to be from cache at the blockHeight

            if(localBlock == null) {
                continue;
            }

            if(block == null || block.getCumulativeDifficulty().compareTo(localBlock.getCumulativeDifficulty()) <= 0) {
                block = localBlock;
            }
        }

        return block;
    }
}
