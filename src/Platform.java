import java.util.concurrent.TimeUnit;

public class Platform implements  Runnable{
    private final State state;
    private boolean quit = false;

    public Platform(State state) {
        this.state = state;
    }

    public void stop() {
        quit = true;
    }

    public void run() {
        long time = java.lang.System.currentTimeMillis();
        boolean speedup = true;

        while(!quit) {
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {/* empty*/}

            if(speedup) {
                time += 1000;
                state.setTime(time);
            } else {
                state.setTime(java.lang.System.currentTimeMillis());
            }
        }

    }

}
