import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class C0 {
 public static void main(String[] args) throws Exception{
  State state = new State(0, 0, 0);
  Platform platform = new Platform(state);
  Thread platformThread = new Thread(platform);

  platformThread.start();

  List<Thread> listGeneratorThreads = new ArrayList<>();
  List<Generator> listGenerators = new ArrayList<>();

/*
  for(int i = 0; i < 4; i++) {
   Generator generator;

   generator = new Generator(state, 100 * 1000 * 1000);

   listGenerators.add(generator);

   Thread thread = new Thread(generator);
   listGeneratorThreads.add(thread);

   thread.start();
  }
*/
///*
  for(int i = 0; i < 1100; i++) {
   Generator generator;

   if (i == 0) {
    generator = new Generator(state, 100 * 1000 * 1000);
   } else if (i < 100) {
    generator = new Generator(state, 1 * 1000 * 1000);
   } else {
    generator = new Generator(state, 1 * 1000);
   }

   listGenerators.add(generator);

   Thread thread = new Thread(generator);
   listGeneratorThreads.add(thread);

   thread.start();
  }
//*/
  try {
   TimeUnit.MILLISECONDS.sleep(120 * 1000);
  } catch (InterruptedException e) {/* empty*/}

  for (Generator generator : listGenerators) {
//   generator.stop();
  }

  for (Thread generatorThread : listGeneratorThreads) {
   generatorThread.join();
  }

  platform.stop();
  platformThread.join();
 }
}
