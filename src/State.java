import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

public class State {

    private final int timePrecision = 1000;
    private final long epoch = java.lang.System.currentTimeMillis();
    private long epochDelta = 0;
    private int generationTimeTarget = 2 * timePrecision;
    private final int generationTimeMaximum = generationTimeTarget * 2;
    private final int timeStagger = 1 * timePrecision;

    private long platformEffectiveBalance = 1000 * 1000 * 1000;
    private int listBlockEffectiveBalanceHistorySize = 1440;
    private final List<Block> blockchain = new ArrayList<>();

    private final BigInteger difficultyPlatformEffectiveBalanceLimit = BigInteger.ONE.shiftLeft(64).subtract(BigInteger.ONE);
    private final BigInteger generatorValueMaximum = BigInteger.ONE.shiftLeft(64).subtract(BigInteger.ONE);
    private BigInteger difficultyPlatformEffectiveBalanceScale;

    private final BigInteger cumulativeDifficultyDivisor = BigInteger.ONE.shiftLeft(Long.SIZE);
    private final BigInteger UNSIGNED_LONG_MASK = cumulativeDifficultyDivisor.subtract(BigInteger.ONE);

    private long replacementCount = 0;

    public class Block {
        private final byte[] generationHash;
        private final byte[] generatorPublicKey;
        private final long generatorEffectiveBalanceFXT; // can be derived instead of stored and kept in a FIFO buffer.
        private final int height;
        private final long id; // using generationHash, no payload
        private final int time;

        private long cumulativeDifficultyHigh = 0;
        private long cumulativeDifficultyLow = 0;

        public  Block(Block block, byte[] publicKey, long effectiveBalance, long time) {
            synchronized (this) {

                if (block != null) {
                    generationHash = generationHash(block.generationHash, publicKey);
                    setCumulativeDifficulty(block.getCumulativeDifficulty().add(blockGenerationDifficulty(generationHash, publicKey, effectiveBalance)));
                    height = block.height + 1;
                    generatorEffectiveBalanceFXT = effectiveBalance;
                } else {
                    generationHash = new byte[0x20];
                    setCumulativeDifficulty(BigInteger.ZERO);
                    height = 0;
                    generatorEffectiveBalanceFXT = 0;
                }

                if (publicKey != null) {
                    generatorPublicKey = Arrays.copyOf(publicKey, publicKey.length);
                } else {
                    generatorPublicKey = new byte[0x10];
                }

                id = getId();
                this.time = (int) (time / timePrecision);
            }
        }

        public void setCumulativeDifficulty(BigInteger cumulativeDifficulty) {
            BigInteger high = cumulativeDifficulty.shiftRight(Long.SIZE);
            cumulativeDifficultyHigh = high.longValue();
            cumulativeDifficultyLow = cumulativeDifficulty.subtract(high.shiftLeft(Long.SIZE)).longValue();
        }

        public BigInteger getCumulativeDifficulty() {
            return BigInteger.valueOf(cumulativeDifficultyHigh).and(UNSIGNED_LONG_MASK).shiftLeft(Long.SIZE).add(BigInteger.valueOf(cumulativeDifficultyLow).and(UNSIGNED_LONG_MASK));
        }

        public long getGeneratorId() {
            BigInteger bigInteger = new BigInteger(1, new byte[]{generatorPublicKey[7], generatorPublicKey[6], generatorPublicKey[5], generatorPublicKey[4], generatorPublicKey[3], generatorPublicKey[2], generatorPublicKey[1], generatorPublicKey[0]});
            return bigInteger.longValue();
        }

        public long getId() {
            BigInteger bigInteger = new BigInteger(1, new byte[]{generationHash[7], generationHash[6], generationHash[5], generationHash[4], generationHash[3], generationHash[2], generationHash[1], generationHash[0]});
            return bigInteger.longValue();
        }
    }

    public State(long platformEffectiveBalance, int generationTimeTargetMilliseconds, int listBlockEffectiveBalanceHistorySize) {

        synchronized (this) {

            pushBlock(new Block(null, null, platformEffectiveBalance, 0));

            if(platformEffectiveBalance > 0) {
                this.platformEffectiveBalance = platformEffectiveBalance;
            }

            if(generationTimeTargetMilliseconds > 0) {
                this.generationTimeTarget = generationTimeTargetMilliseconds;
            }

            if(listBlockEffectiveBalanceHistorySize > 0) {
                this.listBlockEffectiveBalanceHistorySize = listBlockEffectiveBalanceHistorySize;
            }

            updateDifficultyEffectiveBalanceScale();
        }
    }

    private void updateDifficultyEffectiveBalanceScale() {
        difficultyPlatformEffectiveBalanceScale = generatorValueMaximum.divide(BigInteger.valueOf(platformEffectiveBalance).multiply(BigInteger.valueOf(generationTimeTarget * (long) 2)));
        BigInteger v = generatorValueMaximum.divide(BigInteger.valueOf(platformEffectiveBalance).multiply( difficultyPlatformEffectiveBalanceScale));
        System.out.println("difficultyPlatformEffectiveBalanceScale : " + difficultyPlatformEffectiveBalanceScale + " difficultyPlatformEffectiveBalanceLimit : " + difficultyPlatformEffectiveBalanceLimit + " generationTimeTargetLimit : " + v.longValue());
    }

    private long blockEffectiveBalanceActiveTotalEstimate() {
        long cumulative = 0;

        synchronized (this) {
            TreeMap<Long, Long> generatorList = new TreeMap<>();
            int listSize = blockchain.size();
            int countForCalculation = listSize;

            if(countForCalculation > listBlockEffectiveBalanceHistorySize) {
                countForCalculation = listBlockEffectiveBalanceHistorySize;
            }

            for(int i = listSize - countForCalculation; i < listSize; i++) {
                if(! generatorList.containsKey(blockchain.get(i).getGeneratorId())) {
                    long effectiveBalance = blockchain.get(i).generatorEffectiveBalanceFXT;
                    generatorList.put(blockchain.get(i).getGeneratorId(), effectiveBalance);
                    cumulative = cumulative + effectiveBalance;
                }
            }

            if(countForCalculation == 0) {
                cumulative = platformEffectiveBalance;
            }

            return cumulative;
        }
    }

    private void pushBlock(Block block) {
        synchronized (this) {
            blockchain.add(block);
        }
    }

    public byte[] getGenerationHash() {
        return blockchain.get(blockchain.size() - 1).generationHash;
    }

    public void submitBlock(Block block) {
        if(block == null) {
            return;
        }

        synchronized (this) {

            if(blockchain.size() < block.height) {
                 return;
            }

            Block blockOld;

            try {
                blockOld = blockchain.get(block.height - 1);
            } catch (Exception e) {
                System.out.println("exception " + blockchain.size() + " : " + (block.height - 1));
                return;
            }

            if(blockOld == null) {
                return;
            }

            if (block.time <= epochDelta && (blockOld.getCumulativeDifficulty().compareTo(block.getCumulativeDifficulty()) < 0)) {
                if (blockchain.size() == 0 || (block.height == (blockchain.get(blockchain.size() - 1).height + 1))) {
                    blockPrintToConsole(block, getBlockLast());
                    blockchain.add(block);
                } else if (block.height <= (blockchain.get(blockchain.size() - 1).height)) {

                    while (blockchain.size() > block.height) {
                        blockchain.remove(blockchain.size() - 1);
                    }

                    replacementCount++;
                    blockPrintToConsole(block, getBlockLast());
                    blockchain.add(block);
                }
            }
        }
    }

    public Block getBlockByHeight(int blockHeight) {
        synchronized (this) {
            if (blockHeight >= blockchain.size()) {
                return null;
            }

            return blockchain.get(blockHeight);
        }
    }

    public void setTime(long timeNow) {
        epochDelta = timeNow - epoch;
    }

    public Block getBlockLast() {
        synchronized (this) {
            if(blockchain.size() > 0) {
                return blockchain.get(blockchain.size() - 1);
            } else {
                return null;
            }
        }
    }

    public List<Block> getBlocksLast(int countRequest) {
        List<Block> list = new ArrayList<>();

        synchronized (this) {
            int count = countRequest;

            if(blockchain.size() == 0) {
                return list;
            }

            if(blockchain.size() <= countRequest) {
                count = blockchain.size();
            }

            int height = blockchain.size() - 1;

            for(int i = 0; i < count; i++) {
                list.add(blockchain.get(height - i));
            }
        }

        return list;
    }

    public int getBlockHeight() {

        synchronized (this) {
            return getBlockLast().height;
        }
    }

    public long timeToGenerate(byte[] publicKey, long effectiveBalanceFXT) {
        long timeRemaining = 0;

        long blockTimeDelta = epochDelta - (long) getBlockLast().time * timePrecision;

        BigInteger generatorHitTime;

        synchronized (this) {
            generatorHitTime = generatorHitTime(getGenerationHash(), publicKey, effectiveBalanceFXT);
        }

        long hitTime = generatorHitTime.remainder(BigInteger.valueOf(generationTimeMaximum)).longValue();
        long iterations = generatorHitTime.divide(BigInteger.valueOf(generationTimeMaximum)).longValue();

        if(iterations != 0) {
            hitTime = generationTimeMaximum + iterations * timeStagger;
        }

        if(blockTimeDelta < hitTime) {
            timeRemaining = hitTime - blockTimeDelta;
        }

//        System.out.println("height : " + getBlockHeight() + " timeRemaining " + timeRemaining / timePrecision + " generatorHitTime : " + generatorHitTime.longValue() / timePrecision + " publicKey : " + bytesToHex(publicKey) + " effectiveBalanceFXT : " + effectiveBalanceFXT + " total k : " + blockEffectiveBalanceActiveTotalEstimate() / 1000);
        return timeRemaining;
    }

    public Block generateBlock(Block block, byte[] publicKey, long effectiveBalanceFXT) {
        return new Block(block, publicKey, effectiveBalanceFXT, epochDelta);
    }

    public long generatorCountBlocks(long id) {
        long count = 0;

        for(Block block : blockchain) {
            if(block.getGeneratorId() == id) {
                count++;
            }
        }

        return count;
    }

    public void blockPrintToConsole(Block block, Block blockPrevious) {
        long effectiveBalanceEstimate = blockEffectiveBalanceActiveTotalEstimate();
        long generatorBlockCount = generatorCountBlocks(block.getGeneratorId()) + 1;
        long generatorBlockCountExpected = (long) ((block.height) * ((float) block.generatorEffectiveBalanceFXT / effectiveBalanceEstimate));
        long generatorBlockCountDelta = generatorBlockCount - generatorBlockCountExpected;

        System.out.println(block.height + " : generator : " + bytesToHex(block.generatorPublicKey) + " count : " + generatorBlockCount + " total % : " + 100 * (float) generatorBlockCount / (block.height - 1) + " delta % : " + 100 * (float) generatorBlockCountDelta / generatorBlockCount);
        System.out.println("generationHash : " + bytesToHex(block.generationHash));
        System.out.println("cumulativeDifficulty : " + block.getCumulativeDifficulty());
        System.out.println("effectiveBalance : " + block.generatorEffectiveBalanceFXT + " " + (float) block.generatorEffectiveBalanceFXT / effectiveBalanceEstimate);
        System.out.println("blockTime : " + (block.time - blockPrevious.time));
        System.out.println("generatorHitTime : " + generatorHitTime(blockPrevious.generationHash, block.generatorPublicKey, block.generatorEffectiveBalanceFXT).divide(BigInteger.valueOf(timePrecision)));
        System.out.println("blockTimeTarget : " + generationTimeTarget / timePrecision);
        System.out.println("blockTimeTargetMaximum : " + getBlockTimeTargetMaximum() / timePrecision);

        System.out.println("effectiveBalanceActiveEstimate k : " + effectiveBalanceEstimate / 1000);
        System.out.println("averageBlockTime : " + (float) block.time / block.height);
        System.out.println("replacementRate : " + (float) replacementCount / block.height);
        System.out.println(".");
    }

    public byte[] generationHash(byte[] generationHash, byte[] publicKey) {
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {/* empty */}

        assert digest != null;
        digest.update(generationHash);

        return digest.digest(publicKey);
    }

    private BigInteger generatorValue(byte[] generationHash, byte[] publicKey) {
        byte[] generationHashNext = generationHash(generationHash, publicKey);

        return new BigInteger(1, new byte[]{generationHashNext[7], generationHashNext[6], generationHashNext[5], generationHashNext[4], generationHashNext[3], generationHashNext[2], generationHashNext[1], generationHashNext[0]});
    }

    public BigInteger generatorHitTime(byte[] generationHash, byte[] publicKey, long effectiveBalanceFXT) {
        return generatorValue(generationHash, publicKey)
                .multiply(BigInteger.valueOf(1024))
                .multiply(BigInteger.valueOf(blockEffectiveBalanceActiveTotalEstimate()))
                .divide(BigInteger.valueOf(effectiveBalanceFXT).multiply(difficultyPlatformEffectiveBalanceScale).multiply(BigInteger.valueOf(platformEffectiveBalance)))
                .divide(BigInteger.valueOf(1024));
    }

    private BigInteger blockGenerationDifficulty(byte[] generationHash, byte[] publicKey, long effectiveBalanceFXT) {
        return generatorValueMaximum.subtract(generatorValue(generationHash, publicKey).divide(BigInteger.valueOf(effectiveBalanceFXT)));
    }

/*
    private long getAverageBlockTime () {
        long average = 0;

        if(getBlockHeight() != 0)
         average = getBlockLast().time / getBlockHeight();

        if(average < 1) {
            average = 1;
        }

        return average;
    }
*/

    private long getBlockTimeTargetMaximum () {
        return (long) 2 * generationTimeTarget;
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toLowerCase().toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }

        return new String(hexChars);
    }}
